package com.sourceit.myapplication.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

/**
 * Created by Student on 06.02.2018.
 */

public class DataBaseCreator extends SQLiteOpenHelper{
    public static final String DB_NAME="db_name";
    public static final int DB_VERSION=1;

    public  static  class User implements BaseColumns{
        public static final String TABLE_NAME="t_main";
        public static final String MAIN_USER="user_name";
        public static final String MAIN_AGE="user_age";
        public static final String MAIN_CITY="user_city";


    }
    static String SCRIPT_CREATE_TBL_NAME="CREATE TABLE "+User.TABLE_NAME+" ("+
            User._ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+User.MAIN_USER+ " TEXT, "+
            User.MAIN_AGE+ " INTEGER, "+User.MAIN_CITY + " TEXT "+
            ");";


    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
