package com.sourceit.myapplication.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.sourceit.myapplication.model.User;

import java.util.ArrayList;
import java.util.List;

import static com.sourceit.myapplication.DB.DataBaseCreator.User.MAIN_AGE;
import static com.sourceit.myapplication.DB.DataBaseCreator.User.MAIN_CITY;
import static com.sourceit.myapplication.DB.DataBaseCreator.User.MAIN_USER;

/**
 * Created by Student on 06.02.2018.
 */

public class DataBaseMaster {

    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;
    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {

        dbCreator = new DataBaseCreator(context);
        if (database == null || database.isOpen()) {
            database=dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);

        }
        return instance;
    }

    public long insertUser(User user) {

        ContentValues cv = new ContentValues();
        cv.put(MAIN_USER, user.getName());
        cv.put(MAIN_AGE,user.getAge());
        cv.put(MAIN_CITY,user.getCity());
        return database.insert(DataBaseCreator.User.TABLE_NAME, null, cv);
    }

    public List<String> getUsers() {

        String query = " SELECT " + MAIN_USER + " FROM " + DataBaseCreator.User.TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);
        List<String> list = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {


            list.add(cursor.getString(cursor.getColumnIndex(MAIN_USER)));
            cursor.moveToNext();
            Log.d("database", "getUsers: :");
        }
        cursor.close();
        return list;


    }

}
