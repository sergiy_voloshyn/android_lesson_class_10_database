package com.sourceit.myapplication.model;

/**
 * Created by Student on 06.02.2018.
 */

public class User {
    Long id;
    String name;
    Integer age;
    String city;

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    public User(String name, Integer age, String city) {
        this.name = name;
        this.age = age;
        this.city = city;
    }
}
