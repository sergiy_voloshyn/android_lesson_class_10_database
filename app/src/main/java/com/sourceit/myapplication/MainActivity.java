package com.sourceit.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.sourceit.myapplication.DB.DataBaseCreator;
import com.sourceit.myapplication.DB.DataBaseMaster;
import com.sourceit.myapplication.model.User;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        List<User> listUsers = new ArrayList<>();
        listUsers.add(new User("Jack Nicholson", 60, "New-York"));
        listUsers.add(new User("Ralph Fiennes", 40, "Bronx"));
        listUsers.add(new User("Al Pacino", 50, " Los Angeles"));
        listUsers.add(new User("Brad Pitt", 45, "New-York"));
        listUsers.add(new User("Anthony Hopkins", 75, "Margam"));


        DataBaseMaster dataBaseMaster = DataBaseMaster.getInstance(this);

        for (int i = 0; i < listUsers.size(); i++) {

            dataBaseMaster.insertUser(listUsers.get(i));
        }
        TextView textView = (TextView) findViewById(R.id.text_view);
        List<String> resList = dataBaseMaster.getUsers();
        textView.setText(resList.toString());


    }
}
